import React from 'react';
import './App.css';

import { Link, Route } from "wouter";
import Gifs from "./componentes/Gifs";
import Datos1 from "./componentes/Datos1";
import Datos2 from "./componentes/Datos2";
import Chart from "./componentes/Grafica1";
import Chart2 from "./componentes/Grafica2";


function App() {

    return (
        <div className="App">
            <header className="App-header">
                <h1>Sistemas operativos 1</h1>

                <Link to='/gif/gatos' className="links">Hola</Link>
                <Link to='/mensajes1' className="links">Mensajes Servidor 1</Link>
                <Link to='/mensajes2' className="links">Mensajes Servidor 2</Link>
                <Link to='/grafica1' className="links">Grafica Servidor A</Link>
                <Link to='/grafica2' className="links">Grafica Servidor BS</Link>
                <Route
                    component={Gifs}
                    path="/gif/:keyword"/>
                <Route
                    component={Datos1}
                    path="/mensajes1"/>
                <Route
                    component={Datos2}
                    path="/mensajes2"/>
                <Route
                    component={Chart}
                    path="/grafica1"/>
                <Route
                    component={Chart2}
                    path="/grafica2"/>
                {/*Gifs keyword='drift'/>*/}
            </header>
        </div>
    );
}

export default App;
