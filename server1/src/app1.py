from flask import Flask, jsonify, request, Response, json
from bson import json_util
import requests

app = Flask(__name__)


@app.route('/', methods=['GET'])
def get_datos2():
    message = {
        'message': 'Conectado '
    }
    response = json_util.dumps(message)
    return Response(response, mimetype="application/json")


@app.route('/datos', methods=['POST'])
def get_data():
    # Receiving Data
    username = request.json['autor']
    email = request.json['nota']

    if username and email:

        # ----------- CPU -----------
        URL = "http://34.122.233.238:5000/datos2"
        URL2 = "http://34.123.195.45:5000/datos2"
        r = requests.get(url = URL)
        data = r.json()
        data1 = json.loads(data)
        cad1 = '%(free)d' % data1
        # print(cad1)

        r2 = requests.get(url=URL2)
        data2 = r2.json()
        data12 = json.loads(data2)
        cad2 = '%(free)d' % data12
        # print(cad2)

        # ----------- RAM -----------
        URL = "http://34.122.233.238:5000/datos1"
        URL2 = "http://34.123.195.45:5000/datos1"
        r = requests.get(url=URL)
        data = r.json()
        data1 = json.loads(data)
        cad1ram = '%(free)d' % data1
        # print(cad1ram)

        r2 = requests.get(url=URL2)
        data2 = r2.json()
        data12 = json.loads(data2)
        cad2ram = '%(free)d' % data12
        # print(cad2ram)

        # ----------- NUMERO -----------
        URL = "http://34.122.233.238:5000/cantidad"
        URL2 = "http://34.123.195.45:5000/cantidad"
        r = requests.get(url=URL)
        data = r.json()
        # data1 = json.loads(data)
        cad1can = '%(number)d' % data
        #print(cad1can)

        r2 = requests.get(url=URL2)
        data2 = r2.json()
        # data12 = json.loads(data2)
        cad2can = '%(number)d' % data2
        #print(cad2can)

        mensa = ''

        URL = ""
        if int(cad1can) > int(cad2can):
            mensa += 'menor cantidad server2 '
            URL = "http://34.123.195.45:5000/mensajes"  # Servidor 2
        elif int(cad1can) == int(cad2can):

            if int(cad1ram) > int(cad2ram):
                mensa += 'server1 mejor RAM '
                URL = "http://34.122.233.238:5000/mensajes"  # Servidor 1
            elif int(cad1ram) == int(cad2ram):

                if int(cad1) > int(cad2):
                    mensa += 'server1 mejor CPU '
                    URL = "http://34.122.233.238:5000/mensajes"  # Servidor 1
                elif int(cad1) == int(cad2):
                    mensa += 'iguales CPU asignar 1 '
                    URL = "http://34.122.233.238:5000/mensajes"  # Servidor 1
                else:
                    mensa += 'server2 mejor CPU '
                    URL = "http://34.123.195.45:5000/mensajes"  # Servidor 2

            else:
                mensa += 'server2 mejor RAM '
                URL = "http://34.123.195.45:5000/mensajes"  # Servidor 2
        else:
            mensa += 'menor cantidad server1 '
            URL = "http://34.122.233.238:5000/mensajes"  # Servidor 1

        message = {
            'autor': username,
            'nota': email
        }
        x = requests.post(URL, json=message)
        # print(x.text)

    #else:
    message = {
        'message': mensa
    }
    response = json_util.dumps(message)
    return Response(response, mimetype="application/json")



if __name__ == "__main__":
    app.run(host="0.0.0.0", port=80, debug=True)