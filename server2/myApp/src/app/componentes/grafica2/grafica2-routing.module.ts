import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Grafica2Page } from './grafica2.page';

const routes: Routes = [
  {
    path: '',
    component: Grafica2Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Grafica2PageRoutingModule {}
