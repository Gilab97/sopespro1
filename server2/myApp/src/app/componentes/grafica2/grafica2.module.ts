import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Grafica2PageRoutingModule } from './grafica2-routing.module';

import { Grafica2Page } from './grafica2.page';
import {ChartsModule} from "ng2-charts";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Grafica2PageRoutingModule,
    ChartsModule
  ],
  declarations: [Grafica2Page]
})
export class Grafica2PageModule {}
