import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Grafica2Page } from './grafica2.page';

describe('Grafica2Page', () => {
  let component: Grafica2Page;
  let fixture: ComponentFixture<Grafica2Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Grafica2Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Grafica2Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
