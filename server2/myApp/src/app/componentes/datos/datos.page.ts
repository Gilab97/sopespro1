import { Component, OnInit } from '@angular/core';
import {DatosService} from "../../servicios/datos.service";
import {GlobalService} from "../../servicios/global.service";

@Component({
  selector: 'app-datos',
  templateUrl: './datos.page.html',
  styleUrls: ['./datos.page.scss'],
})
export class DatosPage implements OnInit {

  listaproductos: any

  constructor(private datosService: DatosService,
              private global: GlobalService) { }

  ngOnInit() {
      this.global.fas3 = 0
      console.log(`Iniciar Datos Server 1 ${this.global.fas3}`)
      this.cargar()
  }

    async cargar(){
        await delay(2000);
        this.datosService.getdatos1().subscribe(
            res =>{
                console.log(res);
                this.listaproductos = res
            },error => console.error(error)
        );
        await delay(2000);
        console.log(`Datos Server 1 ${this.global.fas3}`)
        if (this.global.fas3 == 1){
            return 0
        }else {
            await this.cargar()
        }
    }

    change(){
        this.global.fas3 = 1
        console.log(`Detener Datos Server 1 ${this.global.fas3}`)
    }
}

function delay(ms: number) {
    return new Promise( resolve => setTimeout(resolve, ms) );
}
