import { Component, OnInit } from '@angular/core';
import {DatosService} from "../../servicios/datos.service";
import {GlobalService} from "../../servicios/global.service";

@Component({
  selector: 'app-datos2',
  templateUrl: './datos2.page.html',
  styleUrls: ['./datos2.page.scss'],
})
export class Datos2Page implements OnInit {

  listaproductos: any

  constructor(private datosService: DatosService,
              private global: GlobalService) { }

  ngOnInit() {
    this.global.fas4 = 0
    console.log(`Iniciar Datos Server 2 ${this.global.fas4}`)
    this.cargar()
  }

  async cargar(){
    await delay(2000);
    this.datosService.getdatos2().subscribe(
        res =>{
          console.log(res);
          this.listaproductos = res
        },error => console.error(error)
    );
    await delay(2000);
    console.log(`Datos Server 2 ${this.global.fas4}`)
    if (this.global.fas4 == 1){
      return 0
    }else {
      await this.cargar()
    }
  }

  change(){
    this.global.fas4 = 1
    console.log(`Detener Datos Server 2 ${this.global.fas4}`)
  }
}

function delay(ms: number) {
  return new Promise( resolve => setTimeout(resolve, ms) );
}
