import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import { Router } from '@angular/router';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class DatosService {
  private API = 'http://34.122.233.238:5000/';
  private API2 = 'http://34.123.195.45:5000/';
  constructor(private http: HttpClient) { }

  public getdatos1s1() {
    return this.http.get(`${this.API}datos1`);

  }

  public getdatos2s1() {
    return this.http.get(`${this.API}datos2`);
  }

  public getdatos1s2() {
    return this.http.get(`${this.API2}datos1`);
  }

  public getdatos2s2() {
    return this.http.get(`${this.API2}datos2`);
  }

  public getdatos1() {
    return this.http.get(`${this.API}mensajes`);
  }

  public getdatos2() {
    return this.http.get(`${this.API2}mensajes`);
  }
}
