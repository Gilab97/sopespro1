import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'folder/:id',
    loadChildren: () => import('./folder/folder.module').then( m => m.FolderPageModule)
  },
  {
    path: 'grafica',
    loadChildren: () => import('./componentes/grafica/grafica.module').then(m => m.GraficaPageModule)
  },
  {
    path: 'grafica2',
    loadChildren: () => import('./componentes/grafica2/grafica2.module').then(m => m.Grafica2PageModule)
  },
  {
    path: 'datos',
    loadChildren: () => import('./componentes/datos/datos.module').then(m => m.DatosPageModule)
  },
  {
    path: 'datos2',
    loadChildren: () => import('./componentes/datos2/datos2.module').then(m => m.Datos2PageModule)
  },
  {
    path: 'home',
    loadChildren: () => import('./componentes/home/home.module').then(m => m.HomePageModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
